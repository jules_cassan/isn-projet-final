#include <SoftwareSerial.h>
SoftwareSerial mavoieserie(0,1);
#define ALARME 7 // Ici nous définissons les différentes variables que nous utiliserons pour le fonctionnement de la voiture 
#define LOUPIOTE 9
#define CHRONO_LOUPIOTE 3

int chrono_loupiote=CHRONO_LOUPIOTE;
int loupiote_etat=HIGH;

// Vitesse du son sur un aller-retour d'1cm 
#define VITESSE_SON 59 

#define CAPTEUR_DROIT_ENVOI 6 //Ici nous définissons les branches des différents capteurs et moteurs , c'est à dire envoie et réception (pour les capteurs) et puissance et états (pour les moteurs)
#define CAPTEUR_DROIT_RECEPTION 5

#define CAPTEUR_GAUCHE_ENVOI 10
#define CAPTEUR_GAUCHE_RECEPTION 13

#define MOTEUR_DROIT_ENABLE 3
#define MOTEUR_DROIT_INPUT1 2 
#define MOTEUR_DROIT_INPUT2 4

#define MOTEUR_GAUCHE_ENABLE 8
#define MOTEUR_GAUCHE_INPUT1 11
#define MOTEUR_GAUCHE_INPUT2 12

#define AVANCER 1
#define ARRETER 0
#define RECULER -1

#define DROITE 1
#define GAUCHE 2

// Fait avancer, reculer ou arrête un moteur dont on précise les broches en paramètre
// enable, input1, input2 : trois broches du moteur
// action : Action à réaliser : 1=avancer, 0=arrêter, -1=reculer
// puissance : Puissance de rotation du moteur. Valable uniquement pour les actions avancer et reculer.
void moteur_piloter(int enable, int input1, int input2, int action, int puissance){
  analogWrite(enable,map(puissance,0,100,0,255)); // Cette ligne nous permet de directement faire la conversion en pourcentage , 100 correspondant à 255
  if(action==AVANCER){
      digitalWrite(input1,1); // ici nous décidons d'envoyer du courant dans la branche input pour faire tourner les roues de la voiture dans le sens des aiguilles d'une montre 
      digitalWrite(input2,0);
  }
  else if (action==RECULER){ // de même pour reculer , il nous suffit d'inverser les valeurs d'input 1 et 2
      digitalWrite(input1,0);
      digitalWrite(input2,1);
  }
  else{
      digitalWrite(input1,0); // afin que la voiture puisse s'arrêter , nous ne lui envoyons pas de courant dans ses branches
      digitalWrite(input2,0);
  }
}

// On doit envoyer des informations dans les trois broches du L293DNE pour piloter le moteur droit
// donc ce sont des sorties sur l'arduino
void moteur_droit_initialiser(){
    pinMode(MOTEUR_DROIT_ENABLE,OUTPUT);
    pinMode(MOTEUR_DROIT_INPUT1,OUTPUT); 
    pinMode(MOTEUR_DROIT_INPUT2,OUTPUT);
}

// Même chose pour le moteur gauche que pour le moteur droit, seules les broches sont différentes
void moteur_gauche_initialiser(){
    pinMode(MOTEUR_GAUCHE_ENABLE,OUTPUT);
    pinMode(MOTEUR_GAUCHE_INPUT1,OUTPUT); 
    pinMode(MOTEUR_GAUCHE_INPUT2,OUTPUT);
}

void moteur_droit_piloter(int action,int puissance){
    moteur_piloter(MOTEUR_DROIT_ENABLE, MOTEUR_DROIT_INPUT1, MOTEUR_DROIT_INPUT2,action,puissance);
}

void moteur_gauche_piloter(int action,int puissance){
    moteur_piloter(MOTEUR_GAUCHE_ENABLE, MOTEUR_GAUCHE_INPUT1, MOTEUR_GAUCHE_INPUT2,action,puissance);
}

void voiture_tourner(int direction){
  if(direction == GAUCHE){
    moteur_droit_piloter(AVANCER,0); // Nous avons décider de faire tourner une seule roue lorsque la voiture tourne 
    moteur_gauche_piloter(AVANCER,100);
  }
  else{
    moteur_droit_piloter(AVANCER,100);
    moteur_gauche_piloter(AVANCER,0);
  }
//  delay(250);
}

void voiture_arreter(){
  moteur_droit_piloter(AVANCER,0);
  moteur_gauche_piloter(AVANCER,0);
}

void voiture_avancer(){
  moteur_droit_piloter(AVANCER,90); // afin de compenser l'écart de vitesse des moteurs nous sommes obligés de mettre le moteur droit à 90 pour cent de sa puissance 
  moteur_gauche_piloter(AVANCER,100);
}

void capteur_droit_initialiser(){
  pinMode(CAPTEUR_DROIT_RECEPTION,INPUT); // La variable est une entrée car c'est elle qui réceptionne l'ultrason
  pinMode(CAPTEUR_DROIT_ENVOI,OUTPUT); // Par conséquent la variable envoi est une sortie
}

void capteur_gauche_initialiser(){
  pinMode(CAPTEUR_GAUCHE_RECEPTION,INPUT); // La variable est une entrée car c'est elle qui réceptionne l'ultrason
  pinMode(CAPTEUR_GAUCHE_ENVOI,OUTPUT); // Par conséquent la variable envoi est une sortie
}

unsigned long capteur_gauche_lire(){
  digitalWrite(CAPTEUR_GAUCHE_ENVOI,LOW); // Ici on essaye d'avoir un signal clair alors on coupe l'émetteur d'ultrasons en le mettant sur low
  delayMicroseconds(2);
  digitalWrite(CAPTEUR_GAUCHE_ENVOI,HIGH); // là on envoie un signal pendant très peu de temps 
  delayMicroseconds(10);
  digitalWrite(CAPTEUR_GAUCHE_ENVOI,LOW); // là on coupe le signal
  return pulseIn(CAPTEUR_GAUCHE_RECEPTION,HIGH) / VITESSE_SON; // afin d'avoir une mesure en cm , nous devons diviser cette mesure par la vitesse du son sur 1 cm
}


unsigned long capteur_droit_lire(){
  digitalWrite(CAPTEUR_DROIT_ENVOI,LOW); // Ici on essaye d'avoir un signal clair alors on coupe l'émetteur d'ultrasons en le mettant sur low
  delayMicroseconds(2);
  digitalWrite(CAPTEUR_DROIT_ENVOI,HIGH); // là on envoie un signal pendant très peu de temps 
  delayMicroseconds(10);
  digitalWrite(CAPTEUR_DROIT_ENVOI,LOW); // là on coupe le signal
  return pulseIn(CAPTEUR_DROIT_RECEPTION,HIGH) / VITESSE_SON; // afin d'avoir une mesure en cm , nous devons diviser cette mesure par la vitesse du son sur 1 cm
}
String decodage()
{
    char recvChar; // nous déclarons cette variable comme une chaîne de caractère
    String donnee;
    donnee="";
    int lecture=0;
while (mavoieserie.available()) // tant que le modéle bluetooth est disponible alors :

  {
    recvChar=mavoieserie.read(); // Nous récupérons les données envoyés de notre télephone 
    donnee+=recvChar;
    Serial.println(donnee);
  }
  return donnee;
}




void setup() {

  Serial.begin(9600);// Celà nous servira à récuperer des informations nottament la distance entre l'obstacle et le capteur

  moteur_droit_initialiser();
  moteur_gauche_initialiser();

  capteur_droit_initialiser();
  capteur_gauche_initialiser();

  pinMode(ALARME,OUTPUT); // nous déclarons nos dernières variables qui correspondent respectivement à notre piezzo et notre led
  pinMode(LOUPIOTE,OUTPUT);
}

void loupiote_clignoter(){
  chrono_loupiote--;
  if(chrono_loupiote < 0){
    if(loupiote_etat == HIGH) loupiote_etat=LOW;
    else loupiote_etat=HIGH;
    chrono_loupiote=CHRONO_LOUPIOTE;
  }
  digitalWrite(LOUPIOTE,loupiote_etat);
}
void bluetooth()
{
  String donneerecu=decodage(); // Ici nous récuperons les données reçues par l'arduino
if (donneerecu=="Gauche"){ // Toutes ces conditions servent à contrôler la voiture par bluetooth
voiture_tourner(GAUCHE);
delay(1000);
}
else if (donneerecu=="Droite"){
voiture_tourner(DROITE);
delay(1000);
}
else if (donneerecu=="Recule"){
moteur_piloter(0,1,0,RECULER,100);
delay(1000);
}
else if (donneerecu=="Avance"){
voiture_avancer();
delay(1000);
}
else if (donneerecu=="Klaxon"){ 
tone(ALARME,440);
delay(850);
tone(ALARME,293.66);
delay(850);
tone(ALARME,349.23);
delay(250);
tone(ALARME,392);
delay(250);
tone(ALARME,440);
delay(500);
tone(ALARME,293.66);
delay(500);
tone(ALARME,349.23);
delay(250);
tone(ALARME,392);
delay(250);
tone(ALARME,329.63);
delay(1000);
}
else if (donneerecu=="Stop"){
voiture_arreter();
delay(1000);
}
}
void loop() {
  loupiote_clignoter();

  unsigned long distance_obstacle_gauche=capteur_gauche_lire(); // cela nous permet d'obtenir précisement la distance entre la voiture et l'obtsacle
  Serial.print(distance_obstacle_gauche); // Ici nous récuperons cette distance sur le moniteur série afin de voir où se trouve l'obstacle
  Serial.println("cm à gauche");

  unsigned long distance_obstacle_droit=capteur_droit_lire();
  Serial.print(distance_obstacle_droit); // Ici nous récuperoons cette distance sur le moniteur série afin de voir où se trouve l'obstacle
  Serial.println("cm à droite");

  if (mavoieserie.available()){ // Ici si notre appareil est connecté à la carte arduino alors la voiture passe de voiture autonome à complètement contrôlable
    bluetooth();
  }
    
  if(distance_obstacle_gauche<=30 && distance_obstacle_gauche!= 0 ){
    tone(ALARME,490); // afin de nous aider dans nos divers tests nous faisons sonner la voiture lorsqu'elle se trouve proche d'un obstacle , la voiture possède deux sons différents pour chaque capteur
    voiture_tourner(DROITE);
  }
  else if(distance_obstacle_droit<=30 && distance_obstacle_droit!= 0){
    tone(ALARME,225);
    voiture_tourner(GAUCHE);
  }
  else{
      noTone(ALARME);//Dans le cas où la voiture ne voit aucun obstacle , l'alarme se coupe et elle continue son trajet
      voiture_avancer();
     }
}
 
